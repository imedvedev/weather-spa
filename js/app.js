var app = angular.module("weather", [
	"ngSanitize",
	"LocalStorageModule",
	"ngCities",
	"ngWeather",
	"ngGeolocation",
	"ngGeo",
	"angularModalService"
]);

app.config(function(localStorageServiceProvider) {
	localStorageServiceProvider.setPrefix("weather").setStorageType("localStorage");
});