/**
 * MainController
 */
angular.module("weather")
.controller("MainController", function($scope, $rootScope, ngCities, ngWeather, ngGeo, ModalService, $timeout) {
	// Города
	$scope.myCities = ngCities.getMyCities();

	// Геолокация
	$scope.geolocation = ngGeo;

	// Погода
	$scope.selectedUnit = ngWeather.getUnit();
	$scope.forecast = [];
	
	// Получение прогноза
	$scope.loadingForecast = false;
	$rootScope.getForecast = function() {
		$scope.myCities = ngCities.getMyCities();
		$scope.forecast = [];

		var loaded = 0;
		angular.forEach($scope.myCities, function(city) {
			$scope.loadingForecast = true;

			ngWeather.getWeather(city).then(function(forecast) {
				forecast.city = city;
				forecast.unit = ngWeather.getUnit().sign;
				forecast.icon = ngWeather.getIcon(forecast.weather[0].icon);
				$scope.forecast.push(forecast);

				loaded ++;
				if(loaded == $scope.myCities.length) {
					$scope.loadingForecast = false;
				}
			});
		});
	}

	$rootScope.getForecast();

	// Выбор города
	$scope.openCityList = function() {
		ModalService.showModal({
			template: [
				'<div class="modal">',
					'<div class="modal-inner">',
						'<div class="close" ng-click="close()"><i class="fa fa-remove"></i></div>',
						'<h3>Список городов:</h3>',
						'<label class="list-label" ng-repeat="city in list"><input type="checkbox" ng-model="city.selected" ng-value="city.code" /> <span ng-bind-html="city.name"></span></label>',
						'<button ng-click="save()">Сохранить</button>',
					'</div>',
				'</div>'
			].join(""),
			controller: function($scope, close) {
				$scope.list = ngCities.citiesList;
				$scope.myCities = ngCities.getMyCities();

				angular.forEach($scope.myCities, function(v) {
					angular.forEach($scope.list, function(c) {
						if(c.code == v.code) {
							c.selected = true;
						}
					});
				});

				$scope.close = close;

				$scope.save = function() {
					var filtered = $scope.list.filter(function(c) {
						return c.selected;
					});

					ngCities.updateCities(filtered);
					close();
				}
			},
			controllerAs: "cities",
		}).then(function(modal) {
			$timeout(function() {
				modal.element.addClass("show");
			}, 100);

			modal.close.then(function(result) {
				// После закрытия модалки обновляем погоду
				$rootScope.getForecast();
			});
		});
	};

	// Выбор единиц
	$scope.openUnitsList = function() {
		ModalService.showModal({
			template: [
				'<div class="modal">',
					'<div class="modal-inner">',
						'<div class="close" ng-click="close()"><i class="fa fa-remove"></i></div>',
						'<h3>Единицы измерения:</h3>',
						'<label class="list-label" ng-repeat="unit in unitsList"><input type="radio" ng-model="selectedUnit.code" ng-value="unit.code" /> <span ng-bind-html="unit.name"></span></label>',
						'<button ng-click="save()">Сохранить</button>',
					'</div>',
				'</div>'
			].join(""),
			controller: function($scope, close) {
				$scope.unitsList = ngWeather.unitsList;
				$scope.close = close;

				$scope.selectedUnit = ngWeather.getUnit();

				$scope.save = function() {
					ngWeather.setUnit($scope.selectedUnit);
					close();
				}
			},
			controllerAs: "units",
		}).then(function(modal) {
			$timeout(function() {
				modal.element.addClass("show");
			}, 100);

			modal.close.then(function(result) {
				// После закрытия модалки обновляем погоду
				$rootScope.getForecast();
			});
		});
	};

});