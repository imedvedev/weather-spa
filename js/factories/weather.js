/*
 * Weather
 */

angular
.module("ngWeather", [])
.factory("ngWeather", function($http, localStorageService, $q) {

	var weather = {
		key: "266954867c5b009d783383056e52b2ee",
		endpoint: "https://api.openweathermap.org/data/2.5/weather",
		iconPath: "https://openweathermap.org/img/w/",
		unitsList: [
			{ name: "Метрическая система", code: "metric", sign: "°C" },
			{ name: "Английская система", code: "imperial", sign: "°F" },
		],

		// Единицы измерения
		getUnit: function() {
			var jsondata = localStorageService.get("unit"),
				data = angular.copy(this.unitsList[0]);

			if(jsondata) {
				data = JSON.parse(jsondata);
			}

			return data;
		},

		// Выбор единицы измерения
		setUnit: function(unit) {
			angular.forEach(this.unitsList, function(v) {
				if(v.code == unit.code) {
					localStorageService.set("unit", JSON.stringify(v));
				}
			});
		},

		// Получаем ссылку на запрос к апи
		createEndpoint: function(cityName) {
			var self = this,
				url = new URL(self.endpoint);

			url.searchParams.set("q", cityName);
			url.searchParams.set("appid", self.key);

			var unit = self.getUnit();
			url.searchParams.set("units", unit.code);

			return url.toString();
		},

		// Получаем иконку
		getIcon: function(code) {
			return this.iconPath + code + ".png";
		},

		// Метод получения погоды
		getWeather: function(city) {
			var self = this,
				deferred = $q.defer();

			$http
			.get(self.createEndpoint(city.code))
			.then(function(response) {
				if(response) {
					deferred.resolve(response.data);
				} else {
					deferred.reject();
				}
			}, function(err) {
				deferred.reject(err);
			});

			return deferred.promise;
		}
	};

	return weather;
});