/*
 * Cities
 */

angular
.module("ngCities", [])
.factory("ngCities", function(localStorageService) {

	var cities = {

		// Список городов
		citiesList: [
			{ name: "Барнаул", code: "Barnaul" },
			{ name: "Новосибирск", code: "Novosibirsk" },
			{ name: "Москва", code: "Moscow" },
		],

		// Список городов пользователя
		getMyCities: function() {
			var jsondata = localStorageService.get("cities"),
				data = [];

			if(jsondata) {
				data = JSON.parse(jsondata);
			}

			return data;
		},

		// Обновление списка городов
		updateCities: function(cities) {
			localStorageService.set("cities", JSON.stringify(cities));
		}

	};

	return cities;
});