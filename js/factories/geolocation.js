/*
 * Geolocation
 */

angular
.module("ngGeo", [])
.factory("ngGeo", function($rootScope, $http, $geolocation, ngCities) {

	var weather = {
		geolocation: null,
		gettingLocation: false,
		city: null,

		getLocation: function() {
			var self = this;
			self.gettingLocation = true;

			$geolocation.getCurrentPosition()
			.then(function(position) {
				self.geolocation = position;

				// Получаем город из координат
				var url = new URL("https://maps.googleapis.com/maps/api/geocode/json?sensor=false");
				url.searchParams.set("latlng", self.geolocation.coords.latitude + "," + self.geolocation.coords.longitude);
				$http.get(url.toString()).then(function(response) {
					var data = response.data;

					// Выбираем название города
					angular.forEach(data.results[0].address_components, function(ac, i) {
						if(ac && ac.types && ac.types[0] == "locality") {
							var city = {
								name: ac.long_name,
								code: transl(ac.long_name)
							};

							// Полученный город
							self.city = city;
							var list = ngCities.citiesList;

							angular.forEach(list, function(c) {
								if(c.code == city.code) {
									c.selected = true;
								}
							});

							var filtered = list.filter(function(c) {
								return c.selected;
							});

							if(filtered && filtered.length) {
								ngCities.updateCities(filtered);
							} else {
								alert("Вашего города нет в списке.");
							}

							$rootScope.getForecast();

							self.gettingLocation = false;
						}
					});
				});
			}).catch(function() {
				alert("Мы не смогли определить вашу геолокацию.");
				self.gettingLocation = false;
			});
		}
	};

	return weather;
});