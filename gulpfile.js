var gulp = require("gulp");
var concat = require("gulp-concat");
var mainBowerFiles = require("main-bower-files");
var less = require("gulp-less");
var minifyCSS = require("gulp-minify-css");

gulp.task("bower", function() {
    return gulp.src(mainBowerFiles())
    .pipe(concat("vendor.min.js"))
	.pipe(gulp.dest(__dirname + "/js/"))
});


gulp.task("css", function() {
	return gulp.src(__dirname + "/css/style.less")
	.pipe(less())
	.pipe(concat("style.css"))
	.pipe(minifyCSS())
	.pipe(gulp.dest(__dirname + "/css/"));
})

var paths = {
	js: [
		__dirname + "/js/app.js",
		__dirname + "/js/factories/cities.js",
		__dirname + "/js/factories/weather.js",
		__dirname + "/js/factories/geolocation.js",
		__dirname + "/js/controllers/main.js",
	]
};

gulp.task("js", function() {
	return gulp.src(paths.js)
	.pipe(concat("app.min.js"))
	.pipe(gulp.dest(__dirname + "/js/"))
});

gulp.task("default", ["bower", "css", "js"]);